To get started:

1. Open /src/Theme/OxbowThemeNegotiator.php and update the node type and theme that you want to use with this module.  By default, the node type "page" will use the "starter" theme when editing Gutenberg content.

2. To build custom Gutenberg components, execute npm install in the root of the module.  Then, npm run to see available commands including build and watch scripts.
